import React,{Component} from 'react';
import './App.css';
import guests from "./guests.json"

const ARRIVED = "Arrived";
const NOT_ARRIVED = "Not arrived";
export default class GuestsList extends Component{
  constructor(props) {
    super(props);
    this.state = {guests:this.validateGuestsList(guests),query:"",filter:null}
    this.validateGuestsList = this.validateGuestsList.bind(this);
  }
  validateGuestsList(guestsList){
    return guestsList.map(item => ({...item,isArrived:false}));
  }
  changeGuestStatus = (guestId) => () =>{
    const{guests} = this.state;

    const changedGuest =  guests.map(guest =>{
      if(guest._id === guestId){
        guest.isArrived = !guest.isArrived;
      }
      return guest;
    });
    this.setState({guests:changedGuest});
  }
  queryOnTextChange = (e) => {
    this.setState({query:e.target.value});
    this.querySuggestion(e);
  }
  querySuggestion = (e) => {
    const {guests} = this.state;
    const query = e.target.value;
    const results = guests.filter(guest => guest.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
    console.log("[filter]",results);
    this.setState({filter:results});
  }
  render() {
    const{filter,query,guests} = this.state;
    let guestsList;
    if(filter !== null && query.length > 0)
      guestsList = filter;
    else
      guestsList = guests;
    return(
        <div>
          <h1>Guests list</h1>
          <input type="text" value={this.state.query} onChange={this.queryOnTextChange}/>
         <GuestsListRender guests={guestsList} changeGuestStatus={this.changeGuestStatus}/>
        </div>
    );
  }
}

class GuestsListRender extends Component{
  constructor(props) {
    super(props);
    this.state = {guestRenderAmount:10}
  }
  changeGuestRenderAmount = (e) =>{
    const {guestRenderAmount} = this.state;
    const {guests} = this.props;
    if (guestRenderAmount < guests.length)
     this.setState({guestRenderAmount:guestRenderAmount+10});
  }
  renderAllGuests = (e) => {
    const {guestRenderAmount} = this.state;
    const {guests} = this.props;
    if (guestRenderAmount < guests.length)
      this.setState({guestRenderAmount: guests.length});
  }
  render() {
    const{guests} = this.props;
    const{guestRenderAmount} = this.state;
    return(
        <div>
          {guests.length !== 0 ?(
              <>
              <h2>{"Count of matches: " + guests.length}</h2>
              <ul>
                {
                  guests.map((guest,index) => {
                    if(index >= guestRenderAmount)
                      return;
                    return(
                        <li key={index}>
                          <div className={guest.isArrived?"arrived-guest":"not-arrived-guest"}>
                            {guest.name}
                          </div>
                          <div>
                            <input type="button" value={guest.isArrived?ARRIVED:NOT_ARRIVED} onClick={this.props.changeGuestStatus(guest._id)}/>
                          </div>
                        </li>
                    );
                  })
                }
              </ul>
                <input type="button" value="load more 10 guests" onClick={this.changeGuestRenderAmount}/>
                <input type="button" value="load all" onClick={this.renderAllGuests}/>
              </>
          ):<h2>No mathces</h2>}
        </div>

    );
  }
}


